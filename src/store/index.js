import { configureStore, createSlice } from '@reduxjs/toolkit'

let dialog = createSlice({
  name: 'dialog',
  initialState: {
    state: false,
    valable: null
  },
  reducers: {
    dialogShow(state, action) {
      state.state = action.payload.state;
      state.valable = action.payload.valable;
    },
    dialogHide(state) {
      state.state = false;
      state.valable = null;
    }
  }
})

export let { dialogShow, dialogHide } = dialog.actions;

export default configureStore({
	reducer: {
    dialog: dialog.reducer
  }
})