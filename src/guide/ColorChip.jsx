import style from './guide.module.scss';
import { useDispatch } from "react-redux";
import { dialogShow } from '../store';

export default function ColorChip({color, valable}) {
  const dispatch = useDispatch();
  const copyValable = () => {
    dispatch(dialogShow({
      state: true,
      valable: valable
    }));
    navigator.clipboard.writeText(valable);
  };
  return (
    <button className={style['colorChip']} onClick={copyValable}>
      <span style={{backgroundColor: color}}></span>
      {color}
    </button>
  )
}