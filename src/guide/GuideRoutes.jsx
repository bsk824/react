import { Route, Routes } from "react-router-dom";
import GuideInfo from "./GuideInfo";
import GuideFileList from "./GuideFileList";
import GuideComponent from "./GuideComponent";

export default function GuideRoutes() {
  return (
    <Routes>
      <Route index element={<GuideInfo />} />
      <Route path="/filelist" element={<GuideFileList />} />
      <Route path="/component" element={<GuideComponent />} />
    </Routes>
  )
}