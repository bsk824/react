import React from 'react';
import style from './guide.module.scss';
import { NavLink } from 'react-router-dom';
import GuideRoutes from './GuideRoutes';
import GuideDialog from './GuideDialog';

export default function Guide() {
  return (
    <div className={style['wrap']}>
      <header className={style['header']}>
        <h1 className={style['logo']}><NavLink to='/guide/'>GUIDE</NavLink></h1>
        <nav className={style['nav']}>
          <NavLink className={({isActive}) => isActive ? 'isActive' : ''} to='/guide/filelist'>Filelist</NavLink>
          <NavLink className={({isActive}) => isActive ? 'isActive' : ''} to='/guide/component'>Component</NavLink>
        </nav>
      </header>
      <div className={style['container']}>
        <GuideRoutes />
      </div>
      <GuideDialog />
    </div>
  )
};
