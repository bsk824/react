import style from './guide.module.scss';
import { dialogHide } from '../store';
import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from 'react';

export default function GuideDialog() {
  const dispatch = useDispatch();
  const dialog = useSelector((state)=>{
    return state.dialog;
  });
  const [showState, setShow] = useState(false);
  useEffect(()=>{
    if((dialog.state)) {
      setShow(true);
      const hideClass = setTimeout(()=>{
        setShow(false);
      }, 2700);
      const hideTimer = setTimeout(()=>{
        dispatch(dialogHide());
      }, 3000);
      return ()=>{
        clearTimeout(hideClass);
        clearTimeout(hideTimer);
      }
    }
  }, [dialog.valable, dialog.state, dispatch])
  return (
    <>
      {(dialog.state) ? 
        <div className={`${style['dialog']}${showState ? ' isShow' : ''}`}>
          <p>{dialog.valable}</p>
        </div>
      :
        null
      }
    </>
  )
}