import ColorChip from './ColorChip'
import style from './guide.module.scss'
import { color } from './color'

export default function GuideInfo() {
  return (
    <div className={style['table']}>
      <table>
        <tbody>
          <tr>
            <th scope="row">Lang</th>
            <td>React, Scss</td>
          </tr>
          <tr>
            <th scope="row">Font</th>
            <td>
              <pre>{
`NotoSans - 국문
  • 700
  • 500 / default
  • 400
Pretendard - 영문, 숫자
  • 700
  • 500 / default
  • 400`
                }
              </pre>
            </td>
          </tr>
          <tr>
            <th scope="row">Color</th>
            <td>
              <p>컬러칩 클릭 시 컬러 변수 클립보드 복사</p>
              {color.all.map(item=>(
                <ColorChip
                  key={item.color}
                  color={item.color}
                  valable={item.var}
                />
              ))}
              <br /><br />
              <p>Font</p>
              {color.font.map(item=>(
                <ColorChip
                  key={item.color}
                  color={item.color}
                  valable={item.var}
                />
              ))}
              <br /><br />
              <p>공통 클래스로 설정된 폰트</p>
              {color.classFont.map(item=>(
                <ColorChip
                  key={item.color}
                  color={item.color}
                  valable={item.var}
                />
              ))}
              <br /><br />
              <p>icon</p>
              {color.icon.map(item=>(
                <ColorChip
                  key={item.color}
                  color={item.color}
                  valable={item.var}
                />
              ))}
              <br /><br />
              <p>bg</p>
              {color.bg.map(item=>(
                <ColorChip
                  key={item.color}
                  color={item.color}
                  valable={item.var}
                />
              ))}
              <br /><br />
              <p>border</p>
              {color.border.map(item=>(
                <ColorChip
                  key={item.color}
                  color={item.color}
                  valable={item.var}
                />
              ))}
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}
