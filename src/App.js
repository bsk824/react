import { Routes, Route } from 'react-router-dom';
import Guide from './guide/Guide';

export default function App() {
  return (
    <Routes>
      <Route path='/*' element={<Guide/>}></Route>
    </Routes>
  );
}